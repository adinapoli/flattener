export default (anArray) => {
  if (!Array.isArray(anArray)) return [];
  return anArray.flat(Infinity);
};
