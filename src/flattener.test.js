import flattener from './flattener';

describe('Flattener', () => {
  it('should return an empty array if input is a undefined', () => {
    const input = undefined;
    const expected = [];
    const actual = flattener(input);
    expect(actual).toEqual(expected);
  });

  it('should return an empty array if input is a object', () => {
    const input = {};
    const expected = [];
    const actual = flattener(input);
    expect(actual).toEqual(expected);
  });

  it('should return an empty array if input is a string', () => {
    const input = '';
    const expected = [];
    const actual = flattener(input);
    expect(actual).toEqual(expected);
  });

  it('should return an empty array if input is a number', () => {
    const input = 5;
    const expected = [];
    const actual = flattener(input);
    expect(actual).toEqual(expected);
  });

  it('should return a flattened array if input is a flattened array', () => {
    const input = [1, 2, 3];
    const expected = [1, 2, 3];
    const actual = flattener(input);
    expect(actual).toEqual(expected);
  });

  it('should return a flattened array if input is a nested array', () => {
    const input = [[1, 2, [3]], 4];
    const expected = [1, 2, 3, 4];
    const actual = flattener(input);
    expect(actual).toEqual(expected);
  });
});
