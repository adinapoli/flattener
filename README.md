# flattener

## Getting started

Simple utility that can flatten an array of arbitrarily nested arrays of integers into a flat array of integers.

## Install

```
npm i
```

## Test

```
npm run test
```
